package com.bacon.expandablelistview;

import java.util.List;

/**
 * Created by Pallaw Pathak on 20/05/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class DataModel {

    /**
     * code : 1
     * error : true
     * msg : Chapter Details
     * data : [{"id":"111","units":"Listening Practice Test (Sample) ","Chapters":[{"id":"249","chapter":"Listening 1 ","PDF":"http://studyonnet.org/track/Books/249/Cambridge1Listening1.pdf","Audio":"http://studyonnet.org/track/Books/249/Listening1.mp3","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"250","chapter":"Listening 2","PDF":"http://studyonnet.org/track/Books/250/cambridge1Listening2.pdf","Audio":"http://studyonnet.org/track/Books/250/Listening2.mp3","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"251","chapter":"Listening 3","PDF":"http://studyonnet.org/track/Books/251/Cambridge1Listening3.pdf","Audio":"http://studyonnet.org/track/Books/251/Listening3.mp3","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"252","chapter":"Listening 4","PDF":"http://studyonnet.org/track/Books/252/Cambridge1Listening4.pdf","Audio":"http://studyonnet.org/track/Books/252/Listening4.mp3","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"253","chapter":"Answer Listening 1-4","PDF":"http://studyonnet.org/track/Books/253/Cambridge1AnswerListening.pdf","Audio":"","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"254","chapter":"Transcript Listening 1-4","PDF":"http://studyonnet.org/track/Books/254/Cambridge1Listening1-4Transcript.pdf","Audio":"","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""}]},{"id":"117","units":"Practice Test (Today)","Chapters":[{"id":"294","chapter":"Listening Practice Test (Today)","PDF":"http://studyonnet.org/track/Books/294/ieltsfeverlisteningpracticetest1pdf.pdf","Audio":"http://studyonnet.org/track/Books/294/ieltsfeverlisteningpracticetest1mp3.mp3","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""}]}]
     */

    private int code;
    private boolean error;
    private String msg;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 111
         * units : Listening Practice Test (Sample)
         * Chapters : [{"id":"249","chapter":"Listening 1 ","PDF":"http://studyonnet.org/track/Books/249/Cambridge1Listening1.pdf","Audio":"http://studyonnet.org/track/Books/249/Listening1.mp3","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"250","chapter":"Listening 2","PDF":"http://studyonnet.org/track/Books/250/cambridge1Listening2.pdf","Audio":"http://studyonnet.org/track/Books/250/Listening2.mp3","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"251","chapter":"Listening 3","PDF":"http://studyonnet.org/track/Books/251/Cambridge1Listening3.pdf","Audio":"http://studyonnet.org/track/Books/251/Listening3.mp3","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"252","chapter":"Listening 4","PDF":"http://studyonnet.org/track/Books/252/Cambridge1Listening4.pdf","Audio":"http://studyonnet.org/track/Books/252/Listening4.mp3","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"253","chapter":"Answer Listening 1-4","PDF":"http://studyonnet.org/track/Books/253/Cambridge1AnswerListening.pdf","Audio":"","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""},{"id":"254","chapter":"Transcript Listening 1-4","PDF":"http://studyonnet.org/track/Books/254/Cambridge1Listening1-4Transcript.pdf","Audio":"","Audio2":"","Audio3":"","Audio4":"","Audio5":"","Video":"","book_content":""}]
         */

        private String id;
        private String units;
        private List<ChaptersBean> Chapters;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnits() {
            return units;
        }

        public void setUnits(String units) {
            this.units = units;
        }

        public List<ChaptersBean> getChapters() {
            return Chapters;
        }

        public void setChapters(List<ChaptersBean> Chapters) {
            this.Chapters = Chapters;
        }

        public static class ChaptersBean {
            /**
             * id : 249
             * chapter : Listening 1
             * PDF : http://studyonnet.org/track/Books/249/Cambridge1Listening1.pdf
             * Audio : http://studyonnet.org/track/Books/249/Listening1.mp3
             * Audio2 :
             * Audio3 :
             * Audio4 :
             * Audio5 :
             * Video :
             * book_content :
             */

            private String id;
            private String chapter;
            private String PDF;
            private String Audio;
            private String Audio2;
            private String Audio3;
            private String Audio4;
            private String Audio5;
            private String Video;
            private String book_content;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getChapter() {
                return chapter;
            }

            public void setChapter(String chapter) {
                this.chapter = chapter;
            }

            public String getPDF() {
                return PDF;
            }

            public void setPDF(String PDF) {
                this.PDF = PDF;
            }

            public String getAudio() {
                return Audio;
            }

            public void setAudio(String Audio) {
                this.Audio = Audio;
            }

            public String getAudio2() {
                return Audio2;
            }

            public void setAudio2(String Audio2) {
                this.Audio2 = Audio2;
            }

            public String getAudio3() {
                return Audio3;
            }

            public void setAudio3(String Audio3) {
                this.Audio3 = Audio3;
            }

            public String getAudio4() {
                return Audio4;
            }

            public void setAudio4(String Audio4) {
                this.Audio4 = Audio4;
            }

            public String getAudio5() {
                return Audio5;
            }

            public void setAudio5(String Audio5) {
                this.Audio5 = Audio5;
            }

            public String getVideo() {
                return Video;
            }

            public void setVideo(String Video) {
                this.Video = Video;
            }

            public String getBook_content() {
                return book_content;
            }

            public void setBook_content(String book_content) {
                this.book_content = book_content;
            }
        }
    }
}
